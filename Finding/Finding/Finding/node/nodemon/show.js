const fs = require('fs');
const express = require('express');
const path = require('path');
const bodyParser = require('body-parser'); // 导入 body-parser 中间件

const filePath = path.join(__dirname, 'datas', 'text.json');

const show = express.Router();
show.use(bodyParser.json()); // 使用 body-parser 中间件来解析 JSON 数据
show.use(express.static('img'));

show.post('/api/show', (req, res) => {
  const title = req.body.inputtitle; // 使用 req.body 获取传递的值
  const text = req.body.inputText;
  console.log('6'+req.body.inputtitle);
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading file:', err);
      res.status(500).json({ error: 'Error reading file' });
      return;
    }
    try {
      const jsonData = JSON.parse(data);
      const newItem = {
        title: title,
        text: text
      };

      jsonData.push(newItem);
      console.log('newItem');

      const updatedData = JSON.stringify(jsonData, null, 2);

      fs.writeFile(filePath, updatedData, 'utf8', err => {
        if (err) {
          console.error('Error writing file:', err);
          res.status(500).json({ error: 'Error writing file' });
        } else {
          res.json({ message: 'Data added successfully' });
        }
      });
    } catch (error) {
      console.error('Error parsing JSON:', error);
      res.status(500).json({ error: 'Error parsing JSON' });
    }
  });
});
show.get('/api/getdata', (req, res) => {
    fs.readFile(filePath, 'utf8', (err, data) => {
      if (err) {
        console.error('Error reading file:', err);
        res.status(500).json({ error: 'Error reading file' });
        return;
      }
      try {
        const jsonData = JSON.parse(data);
        res.json(jsonData);
      } catch (error) {
        console.error('Error parsing JSON:', error);
        res.status(500).json({ error: 'Error parsing JSON' });
      }
    });
  });
// ...

module.exports = show;
