const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs'); // 导入文件系统模块

const finding = express.Router();


finding.use(bodyParser.json());

finding.use(express.static('img'));


const jsonData = JSON.parse(fs.readFileSync('./datas/find.json', 'utf-8'));

// 处理寻人表单提交的接口
finding.post('/api/submit_form_data', (req, res) => {
  const formData = req.body;
  // 这里可以对 formData 进行进一步处理，比如将数据保存到数据库或进行其他操作
  console.log('Received form data:', formData);

  // 根据标签进行筛选
  let filteredData = jsonData;

  if (formData.selectedTag) {
    // 如果选择了寻人标签，根据选择的标签进行筛选
    filteredData = filteredData.filter((person) => person.selectedTag === formData.selectedTag);
  }

  if (formData.selectedArea && formData.selectedCity) {
    // 如果选择了相关地区（失踪地区），根据选择的地区进行筛选
    filteredData = filteredData.filter(
      (person) => person.selectedArea === formData.selectedArea && person.selectedCity === formData.selectedCity
    );
  }

  if (formData.birthDate) {
    // 如果选择了出生日期，根据选择的出生日期进行筛选
    filteredData = filteredData.filter((person) => person.birthDate === formData.birthDate);
  }

  if (formData.missingDate) {
    // 如果选择了失踪时间，根据选择的失踪时间进行筛选
    filteredData = filteredData.filter((person) => person.missingDate === formData.missingDate);
  }

  if (formData.name) {
    // 如果填写了姓名，根据填写的姓名进行筛选
    filteredData = filteredData.filter((person) => person.name === formData.name);
  }

  if (formData.sex) {
    // 如果选择了性别，根据选择的性别进行筛选
    filteredData = filteredData.filter((person) => person.sex === formData.sex);
  }

  // 将筛选后的结果作为响应发送给前端
  console.log(filteredData);
  res.json(filteredData);
});

module.exports = finding;
