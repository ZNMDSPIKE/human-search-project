const express = require('express');
const loginApp = require('./login');
const leirong = require('./leirong');
const cors = require('cors');
const fingding = require('./findinf');
const comehome = require('./comehome');

const show = require('./show');

const app = express();
app.use(cors());

// 将loginApp作为子应用挂载到主应用中，设置前缀为'/subapp'
app.use('/subapp', loginApp);
// 将leirong作为子应用挂载到主应用中，设置前缀为'/leirong'
app.use('/leirong', leirong);

app.use('/finding', fingding);

app.use('/comehome', comehome);

app.use('/show', show);

const port = 3000;
app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
