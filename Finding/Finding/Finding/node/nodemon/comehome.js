const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs'); // 导入文件系统模块
const path = require('path');
const multer = require('multer');// 导入multer库

const comehome = express.Router();

// 配置存储引擎和文件命名
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'img'); // 指定要保存上传图片的目录
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + '-' + file.originalname); // 设置文件名
    }
});
// 创建一个multer实例，使用上面配置的存储引擎
const upload = multer({ storage: storage });

comehome.use(bodyParser.json());

comehome.use(express.static('img'));


const jsonData = JSON.parse(fs.readFileSync('./datas/find.json', 'utf-8'));
const jsonDataPath = path.join(__dirname, 'datas', 'find.json');
// 处理寻人表单提交的接口
comehome.post('/api/comehome', (req, res) => {
  const formData = req.body;
  // 这里可以对 formData 进行进一步处理，比如将数据保存到数据库或进行其他操作
  console.log('Received form data:', formData);

  // 根据标签进行筛选
  let filteredData = jsonData;


  res.json(filteredData);
});
comehome.post('/api/comehome/tijiao', upload.single('file'), (req, res) => {
    const formData = req.body;
    // 这里可以对 formData 进行进一步处理，比如将数据保存到数据库或进行其他操作
    console.log('提交来的数据是:', formData);

    // 根据标签进行筛选
    if (req.file) {
        console.log('已上传的文件:', req.file);
        formData.tu = req.file.filename;
        // 现在您可以将文件信息保存到您的JSON数据或数据库中
    }else{
        console.log('失败了')
    }

    // 将新的表单数据插入到 JSON 文件的第一个位置
    fs.readFile(jsonDataPath, 'utf-8', (err, data) => {
        if (err) {
            console.error('Error reading JSON file:', err);
            return res.status(500).json({ error: 'Error reading JSON file' });
        }

        const existingData = JSON.parse(data);
        existingData.unshift(formData);  // 使用 unshift 方法将数据插入到数组的开头

        fs.writeFile(jsonDataPath, JSON.stringify(existingData), 'utf-8', (err) => {
            if (err) {
                console.error('Error writing JSON file:', err);
                return res.status(500).json({ error: 'Error writing JSON file' });
            }

            console.log('Form data written to JSON file.');
            res.json(existingData); // 返回更新后的数据
        });
    });
});


module.exports = comehome;
