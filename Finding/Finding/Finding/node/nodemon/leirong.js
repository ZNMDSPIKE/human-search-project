const fs = require('fs');
const express = require('express');
const path = require('path');

const filePath = path.join(__dirname, 'datas', 'find.json');

const leirong = express.Router();

leirong.use(express.static('img'));

leirong.get('/api/images/:number', (req, res) => {
  const { number } = req.params;
  console.log(number);
  fs.readFile(filePath, 'utf8', (err, data) => {
    if (err) {
      console.error('Error reading file:', err);
      res.status(500).json({ error: 'Error reading file' });
      return;
    }
    try {
      const jsonData = JSON.parse(data);

      // Filter the data based on the selectedTag
      const filteredData = jsonData.filter(entry => entry.selectedTag === `${number}`);
      
      res.json(filteredData);
    } catch (error) {
      console.error('Error parsing JSON:', error);
      res.status(500).json({ error: 'Error parsing JSON' });
    }
  });
});

module.exports = leirong;
