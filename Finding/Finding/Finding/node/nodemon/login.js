const express = require('express');
const bodyParser = require('body-parser');

const loginApp = express.Router();

loginApp.use(bodyParser.json());

loginApp.post('/api/login', (req, res) => {
  const { username, password } = req.body;
  console.log(`username: ${username}, password: ${password}`);
  if (username === 'pjw' && password === '123456') {
    const data = {
      success: true,
      message: '登录成功'
    };
    console.log("登录成功");
    res.json(data);
  } else {
    const data = {
      success: false,
      message: '账号或密码错误'
    };
    console.log("登录失败");
    res.json(data);
  }
});

module.exports = loginApp;
