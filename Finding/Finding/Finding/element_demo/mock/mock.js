// mock.js
import Mock from 'mockjs';
// 模拟登录接口
Mock.mock('/login', 'post', (req, res) => {
  // 这里可以根据请求参数进行模拟的处理
  const { username, password } = JSON.parse(req.body);
  if (username === 'pjw' && password === '123456') {
    return {
      success: true,
      message: '登录成功',
    };
  } else {
    return {
      success: false,
      message: '账号或密码错误',
    };
  }
});
