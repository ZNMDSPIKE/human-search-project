const state = {
    isLoggedIn: false,
  };
  
  const mutations = {
    SET_IS_LOGGED_IN(state, value) {
      state.isLoggedIn = value;
    },
  };
  
  const actions = {
    setLoggedIn({ commit }, value) {
      commit('SET_IS_LOGGED_IN', value);
    },
  };
  
  const getters = {
    isLoggedIn: (state) => state.isLoggedIn,
  };
  
  export default {
    namespaced: true, // 设置模块命名空间为true
    state,
    mutations,
    actions,
    getters,
  };