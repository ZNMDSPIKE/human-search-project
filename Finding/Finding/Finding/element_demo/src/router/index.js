// main.js

import Vue from 'vue';
import VueRouter from 'vue-router';
import login from '../components/login.vue'; // 登录组件
import houtai from'../components/houtai.vue'; // 后台界面组件
import findperson from'../components/findperson.vue'; // 后台界面组件
import comeback from'../components/comeback.vue'; // 后台界面组件
import sharing from'../components/sharing.vue'; // 后台界面组件
import logined from'../components/logined.vue'; // 后台界面组件
import jingyan from'../components/jingyan.vue';
import tiao from'../components/tiao.vue';
Vue.config.productionTip = false;



// 定义路由映射关系
const routes = [
  {
    path: '/',
    name: 'login',
    component: login,
  },
  {
    path: '/houtai',
    name: 'houtai',
    component: houtai,
  },
  {
    path: '/findperson',
    name: 'findperson',
    component: findperson,
  },
  {
    path: '/comeback',
    name: 'comeback',
    component: comeback,
  },
  {
    path: '/sharing',
    name: 'sharing',
    component: sharing,
  },
  {
    path: '/logined',
    name: 'logined',
    component: logined,
  },
  {
    path: '/jingyan',
    name: 'jingyan',
    component: jingyan,
  },

  {
    path: '/tiao',
    name: 'tiao',
    component: tiao,
  },
];

// 创建 Vue Router 实例
const router1 = new VueRouter({
  routes,
});
export default router1;
