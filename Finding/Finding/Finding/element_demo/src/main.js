import Vue from 'vue'
import '../mock/mock'; // 引入mock.js
import App from './App.vue'
import store from './store'
import './plugins/element.js'
import ElementUI from 'element-ui';
import router1 from './router/index.js'
// 引入VueRouter插件
import VueRouter from 'vue-router'
// 使用VueRouter插件
Vue.use(VueRouter)

Vue.use(ElementUI);
Vue.config.productionTip = false

new Vue({
  render: h => h(App),
  router:router1,
  store,
}).$mount('#app')
